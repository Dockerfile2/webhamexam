<?php
    // Set exam property variables here:
    switch ($_POST['examElement']) {
        case 2:
            $capitolExamTitle = "Technician";
            $lowercaseExamTitle = "technician";
            $examElement = 2;
            $examQuestions = 35;
            $questionPoolPath = "../pools/technician.json";
            break;
        case 3:
            $capitolExamTitle = "General";
            $lowercaseExamTitle = "general";
            $examElement = 3;
            $examQuestions = 35;
            $questionPoolPath = "../pools/general.json";
            break;
        case 4:
            $capitolExamTitle = "Amateur Extra";
            $lowercaseExamTitle = "amateur extra";
            $examElement = 4;
            $examQuestions = 50;
            $questionPoolPath = "../pools/amateur-extra.json";
            break;
        default:
            $capitolExamTitle = "ERROR";
            $lowercaseExamTitle = "ERROR";
            $examElement = NULL;
            $examQuestions = NULL;
            $questionPoolPath = NULL;
            break;
    }

    function gradeResults()
    {

    }
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $capitolExamTitle ?> Exam</title>
    <link rel="stylesheet" type="text/css" href="../styles/main.css" />
</head>
<body>
<h2><?php echo $capitolExamTitle ?> Exam</h2>
<hr>
<form action='../results/'>
<ol>
<?php
    // Initalize the exam!
    for($question = 0; $question < $examQuestions; $question++)
    {
      echo "
      <li>Index: " . $question . "</li>
      ";
    }
?>
</ol>
</form>
</body>
</html>
