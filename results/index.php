<!DOCTYPE html>
<html>
<head>
    <title>Exam Results</title>
    <link rel="stylesheet" type="text/css" href="../styles/main.css" />
</head>
<body>
<?php
    switch ($_POST['examElement']) {
        case 2:
            echo "<h1>Technician Exam Results</h1>";
            break;
        case 3:
            echo "<h1>General Exam Results</h1>";
            break;
        case 4:
            echo "<h1>Amateur Extra Results</h1>";
            break;
        default:
            echo "<h1>Error: an error has occurred!<h1/>";
            break;
    }
?>
</body>
</html>
